import { cardsArrDelete } from "./cardsArrDelete.js"
import { noItem } from "./getAllCards.js";
import { cardsArray, mainContainer} from "../constants/constants.js";


export async function deleteCard(card, userToken){
    await fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
    method: 'DELETE',
    headers: {
        'Authorization': `Bearer ${userToken}`
    }
    })
    .then(response => {
        if(response.ok) {  
            cardsArrDelete(card);
            console.log(cardsArray);
            if (cardsArray.length === 0) {
                mainContainer.append(noItem);
            }
        } else {
            throw new Error('Failed to delete the card.');
        }
    })
}
