import {ChooseDoctor} from "../classes/Modal.js";
import {createVisit} from "./createVisit.js";


export function chooseDoctor() {
    const doctorSelect = new ChooseDoctor(createVisit)
    doctorSelect.renderDoctorFields()
}