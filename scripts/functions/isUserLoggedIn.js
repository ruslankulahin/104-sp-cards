import { logout } from "./logout.js";
import {chooseDoctor} from "./chooseDoctor.js";
import {getAllCards} from "./getAllCards.js";
import {ModalLogIn} from "../classes/Modal.js";
import { filterRender } from "./filterRender.js";


export function isUserLoggedIn() {
    const buttonLogin = document.querySelector('#buttonLogin')
    const buttonLogout = document.querySelector('#buttonLogout')
    const buttonCreateVisit = document.querySelector('#buttonCreatVisit')
    const textLogin = document.querySelector('.login-invitation-text')
    let keyToken = localStorage.getItem('token')

    if(keyToken) {
        buttonLogout.classList.toggle('hidden');
        buttonLogout.addEventListener('click', logout);
        buttonCreateVisit.classList.toggle('hidden');
        buttonCreateVisit.addEventListener('click', chooseDoctor)
        getAllCards(keyToken);
        filterRender();
    } else {
        textLogin.classList.toggle('hidden');
        buttonLogin.classList.toggle('hidden')
        buttonLogin.addEventListener('click', (event) => {
            event.preventDefault();
            const logInForm = new ModalLogIn()
            logInForm.renderModalLogIn()
        })
        textLogin.addEventListener('click', (event) => {
            event.preventDefault();
            const logInForm = new ModalLogIn()
            logInForm.renderModalLogIn()
        })
    }
}