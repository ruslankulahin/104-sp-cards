import {publishCard} from "./publishCard.js";
import { cardsArray, mainContainer } from "../constants/constants.js"


export const noItem = document.createElement('h2');
noItem.id = 'no-item-text';
noItem.innerHTML = `No items have been added`;


export async function getAllCards(token) {
    const response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    })
    .then(response => response.json())
    .then(cards => {
        cardsArray.push(...cards)
        if(cardsArray.length === 0) {
            mainContainer.append(noItem);
        } else {
            noItem.remove();
            cardsArray.forEach((card) => {
                publishCard(card)
            });
        };
    }).then(console.log(cardsArray))
}