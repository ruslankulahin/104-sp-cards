import {chooseDoctor} from "./chooseDoctor.js";
import {logout} from "./logout.js";
import {getAllCards} from "./getAllCards.js";
import { filterRender } from "./filterRender.js";

export function loginSuccess(token) {
    localStorage.setItem('token', token);
    console.log(token);
    const headerButtons = Array.from(document.querySelectorAll('.button'))
    headerButtons.forEach((button) => {
        button.classList.toggle('hidden')
    })
    const creatVisitBtn = document.querySelector('#buttonCreatVisit')
    creatVisitBtn.addEventListener('click', chooseDoctor)
    const logoutBtn = document.querySelector('#buttonLogout')
    logoutBtn.addEventListener('click', logout)
    const loginText = document.querySelector('.login-invitation-text')
    loginText.remove()
    const modalWrapper = document.querySelector('.modal-wrapper')
    modalWrapper.remove()
    getAllCards(token);
    filterRender()
}