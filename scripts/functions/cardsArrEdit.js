import { cardsArray } from "../constants/constants.js";
import { publishCard } from "./publishCard.js";

export function cardsArrEdit(card) {

    const publishedCard = document.getElementById(card.id)
    publishedCard.remove()
    const cardIndex = cardsArray.findIndex((publishedCard) => publishedCard.id === card.id)
    cardsArray.splice(cardIndex, 1, card)
    publishCard(card)
}