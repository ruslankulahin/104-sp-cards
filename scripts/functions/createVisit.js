import {publishCard} from "./publishCard.js";
import { cardsArray } from "../constants/constants.js";
import { noItem } from "./getAllCards.js";


export async function createVisit() {
    const visitInfo = new FormData(this.modalBody)
    const visitObj = {}
    visitObj.doctor = this.doctorSelect.value
    visitObj.status = 'Open'
    for(const [key, value] of visitInfo.entries()) {
        visitObj[key] = value;
    }
    const token = localStorage.getItem('token')
    await fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(visitObj)
    })
    .then(response => response.json())
    .then(response => {
        noItem.remove();
        publishCard(response)
        cardsArray.push(response)
        console.log(cardsArray)
    })
}