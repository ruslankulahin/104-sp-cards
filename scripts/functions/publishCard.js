import {VisitTherapist} from "../classes/VisitTherapist.js";
import {VisitDentist} from "../classes/VisitDentist.js";
import {VisitCardiologist} from "../classes/VisitCardiologist.js";


export function publishCard({doctor, name, title, description, urgency, bp, bmi, diseases, age, lastDateVisit, status, id}) {
    const container = document.querySelector('.main-content-wrapper')

    if (doctor === 'Dentist') {
        const publishedVisit = new VisitDentist(doctor, name, title, description, urgency, lastDateVisit, status, id);
        publishedVisit.renderVisitDentist(container);
    } else if (doctor === 'Therapist') {
        const publishedVisit = new VisitTherapist(doctor, name, title, description, urgency, age, status, id);
        publishedVisit.renderVisitTherapist(container);
    } else if (doctor === 'Cardiologist') {
        const publishedVisit = new VisitCardiologist(doctor, name, title, description, urgency, bp, bmi, diseases, age, status, id);
        publishedVisit.renderVisitCardiologist(container);
    } else {

    }
}