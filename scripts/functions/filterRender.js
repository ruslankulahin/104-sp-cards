import { urgencyList, visitStatus, cardsArray } from "../constants/constants.js"
import { filterCards } from "./filterCards.js"

export function filterRender() {
    const filterWrapper = document.querySelector('.filter')
    const filterForm = document.createElement('form')
    filterForm.id = 'filter-form'
    const filterNameWrapper = document.createElement('div')
    const filterNameLabel = document.createElement('label')
    const filterName = document.createElement('input')
    filterName.name = "name"
    filterName.id = "search-input"
    const filterStatusWrapper = document.createElement('div')
    const filterStatusLabel = document.createElement('label')
    const filterStatus = document.createElement('select')
    filterStatus.name = "status";
    filterStatus.id = "status-filter";
    const filterUrgencyWrapper = document.createElement('div')
    const filterUrgencyLabel = document.createElement('label')
    const filterUrgency = document.createElement('select')
    filterUrgency.name = "urgency"
    filterUrgency.id = "urgency-filter";


    filterNameLabel.innerText = 'Name or description'
    filterStatusLabel.innerText = 'Status'
    filterUrgencyLabel.innerText = 'Urgency'


    urgencyList.forEach((option) => {
        const urgencyRate = document.createElement('option')
        urgencyRate.value = option
        urgencyRate.textContent = option
        if(urgencyRate.value === '--- Chose the option ---') {
            urgencyRate.setAttribute('selected', '')
        }
        filterUrgency.append(urgencyRate)
    })

    visitStatus.forEach((status) => {
        const statusOption = document.createElement('option')
        statusOption.value = status
        statusOption.textContent = status
        if(statusOption.value === '--- Chose the option ---') {
            statusOption.setAttribute('selected', '')
        }
        filterStatus.append(statusOption)
    })

    filterWrapper.classList.remove('hidden')
    filterNameWrapper.append(filterNameLabel, filterName)
    filterStatusWrapper.append(filterStatusLabel, filterStatus)
    filterUrgencyWrapper.append(filterUrgencyLabel, filterUrgency)
    filterForm.append(filterNameWrapper, filterStatusWrapper, filterUrgencyWrapper)
    filterWrapper.append(filterForm)


    let searchInputValue = document.getElementById('search-input').value;
    let statusFilterValue = document.getElementById('status-filter').value;
    let urgencyFilterValue = document.getElementById('urgency-filter').value;

    console.log(searchInputValue);
    console.log(statusFilterValue);
    console.log(urgencyFilterValue);

    filterStatus.addEventListener('change', (e) => {
        statusFilterValue = e.target.value
        filterCards(searchInputValue, statusFilterValue, urgencyFilterValue)
    });
    filterUrgency.addEventListener('change', (e) => {
        urgencyFilterValue = e.target.value
        filterCards(searchInputValue, statusFilterValue, urgencyFilterValue)
    });
    filterName.addEventListener('input', (e) => {
        searchInputValue = e.target.value
        filterCards(searchInputValue, statusFilterValue, urgencyFilterValue)
    });
    
}
