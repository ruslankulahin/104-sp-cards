import {Edit} from "../classes/Modal.js";
import { editedCardConfirm } from "./editedCardConfirm.js";


export function editCard(card) {
    const modalCardEdit = new Edit(editedCardConfirm)
    modalCardEdit.renderEdit(card)
}