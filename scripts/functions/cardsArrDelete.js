import { cardsArray } from "../constants/constants.js";

export function cardsArrDelete(card) {

    const publishedCard = document.getElementById(card.id)
    publishedCard.remove()
    const cardIndex = cardsArray.findIndex((publishedCard) => publishedCard.id === card.id)
    cardsArray.splice(cardIndex, 1)
    return cardsArray
}