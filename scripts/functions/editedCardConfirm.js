import { cardsArrEdit } from "./cardsArrEdit.js"


export async function editedCardConfirm(card) {
    const token = localStorage.getItem('token')
    const editedInfo = new FormData(this.modalBody)
    const editedCardObj = {}
    editedCardObj.doctor = this.doctorSelect.value
    editedCardObj.id = card.id
    for(const [key, value] of editedInfo.entries()) {
        editedCardObj[key] = value;
    }
   await fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  },
  body: JSON.stringify({
    ...editedCardObj
  })
})
  .then(response => response.json())
  .then(response => {
    cardsArrEdit(response)
    this.closeModal()
  })
}
