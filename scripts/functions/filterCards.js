import { cardsArray, mainContainer, container } from "../constants/constants.js";
import {publishCard} from "./publishCard.js";


export const noFilteredItem = document.createElement('h2');
noFilteredItem.id = 'no-filtered-item-text';
noFilteredItem.innerHTML = `No results found`;


export function filterCards( searchInputValue, statusFilterValue, urgencyFilterValue) {

    console.log(searchInputValue);
    console.log(statusFilterValue);
    console.log(urgencyFilterValue);

    const filteredCards = cardsArray.filter(({ name, description }) => {
        return (
            name.toLowerCase().includes(searchInputValue.toLowerCase()) ||
            description.toLowerCase().includes(searchInputValue.toLowerCase())
        );
    });
    console.log(filteredCards);
    const filterByUrgency = filteredCards.filter((el) => {
        const { urgency } = el;
        if (urgencyFilterValue === "--- Chose the option ---") {
            return true;
        } else if (urgencyFilterValue !== "--- Chose the option ---" && urgencyFilterValue === urgency) {
            return true;
        } else {
            return false;
        }
    });
    console.log({filterByUrgency});

    const filterByStatus = filterByUrgency.filter((el) => {
        const { status } = el;
        if (statusFilterValue === "--- Chose the option ---") {
            return true;
        } else if (statusFilterValue !== "--- Chose the option ---" && statusFilterValue === status) {
            return true;
        } else {
            return false;
        }
    });
    console.log({filterByStatus});
    
    if (filterByStatus.length !== 0) {
        container.innerHTML = "";
        noFilteredItem.remove();
        filterByStatus.forEach((card) => { 
            publishCard(card);
        })
        return filteredCards;
    } else {
        container.innerHTML = "";
        mainContainer.append(noFilteredItem);
        
    }
};



