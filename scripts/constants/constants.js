export const API = 'https://ajax.test-danit.com/api/v2/cards/login'
export const doctorsList = ['--- Chose the option ---', 'Cardiologist', 'Dentist', 'Therapist']
export const urgencyList = ['--- Chose the option ---', 'High', 'Normal', 'Low']
export const visitStatus = ['--- Chose the option ---', 'Open', 'Done']
export const cardsArray = []
export const mainContainer = document.querySelector('.main');
export const container = document.querySelector(".main-content-wrapper");